{% if a; b else c %}

{%

%}

%(  a )

%[ a  ]

%{ a  }

%< a >

lalala
:test
a{"a" => "b", c => true

def a
  puts "hello"
end

"#{if a then b else c end}"

->(a){ 1 }

class Test
end

macro for(expr)
  {{expr.args.first.args.first}}.each do |{{expr.name.id}}|
    {{expr.args.first.block.body}}
  end
end

macro define_method(name, content)
  def {{name}}
    {% if content == 1 %}
      "one"
    {% elsif content == 2 %}
      "two"
    {% else %}
      {{content}}
    {% end %}
  end
end

for i in [1,2,3] do
  puts i
end

[1,2,3].each do |i|
  puts i
end

3.times do [1,2,3]

# note the trailing 'do' as block-opener!

12345678

if a && b
  puts a
else
  puts b
end

Symbol
test do |apple|
def apple

`
a=5
sudo rm -rf
echo $a
`
a=5

''

'a'

def one
  1
end

proc = ->one
proc.call #=> 1
