<?xml version="1.0" encoding="UTF-8"?>
<!--

 This file is part of GtkSourceView

 Author: Archit Baweja <bighead@users.sourceforge.net>
 Copyright (C) 2004 Archit Baweja <bighead@users.sourceforge.net>
 Copyright (C) 2005 Michael Witrant <mike@lepton.fr>
 Copyright (C) 2006 Gabriel Bauman <gabe@codehaus.org>
 Copyright (C) 2013 Jesse van den Kieboom <jessevdk@gnome.org>

 GtkSourceView is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 GtkSourceView is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

-->
<language id="crystal" _name="Crystal" version="2.0" _section="Source">
  <metadata>
    <property name="mimetypes">application/x-crystal;text/x-crystal</property>
    <property name="globs">*.cr</property>
    <!-- <property name="line-comment-start">#</property> -->
  </metadata>

  <styles>
    <style id="escape"               _name="Escaped Character"          map-to="def:special-char"/>
    <style id="comment"              _name="Comment"                    map-to="def:comment"/>
    <style id="attribute-definition" _name="Attribute Definition"       map-to="def:statement"/>
    <style id="module-handler"       _name="Module handler"             map-to="def:preprocessor"/>
    <style id="keyword"              _name="Keyword"                    map-to="def:keyword"/>
    <style id="nil-value"            _name="Nil Constant"               map-to="def:special-constant"/>
    <style id="boolean"              _name="Boolean value"              map-to="def:boolean"/>
    <style id="floating-point"       _name="Floating point number"      map-to="def:floating-point"/>
    <style id="decimal"              _name="Decimal number"             map-to="def:decimal"/>
    <style id="base-n-integer"       _name="Base-N number"              map-to="def:base-n-integer"/>
    <style id="numeric-literal"      _name="Numeric literal"            map-to="def:base-n-integer"/>
    <style id="char"                 _name="Character"                  map-to="def:character"/>
    <style id="string"               _name="String"                     map-to="def:string"/>
    <style id="literal"              _name="Literal"                    map-to="def:special-char"/>
    <!-- Translators: functions that are provided in the language -->
    <style id="builtin"              _name="Builtin"                    map-to="def:type"/>
    <style id="constant"             _name="Constant"                   map-to="def:type"/>
    <style id="symbol"               _name="Symbol"                     map-to="def:string"/>
    <style id="special-variable"     _name="Special Variable"           map-to="def:identifier"/>
    <style id="predefined-variable"  _name="Predefined Variable"        map-to="def:identifier"/>
    <style id="variable"             _name="Variable"                   map-to="def:identifier"/>
    <style id="here-doc"             _name="Heredoc"                    map-to="def:string" />
    <style id="here-doc-bound"       _name="Heredoc Bound"              map-to="def:string"/>
    <style id="regex"                _name="Regular Expression"         map-to="def:identifier"/>
    <style id="macro"                _name="Macro"                      map-to="def:preprocessor"/>
    <style id="attribute"            _name="Attribute"                  map-to="def:preprocessor"/>
    <style id="function"             _name="Function"                   map-to="def:builtin"/>
    <style id="variable-definition"  _name="Shell Variable Definition"  map-to="sh:variable-definition"/>
  </styles>

  <definitions>

    <context id="escape" style-ref="escape">
      <match>\\((0-7){3}|(x[a-fA-F0-9]{2})|(c\S)|([CM]-\S)|(M-C-\S)|.)</match>
    </context>

    <context id="multiline-comment" style-ref="comment" class="comment" class-disabled="no-spell-check">
      <start>^=begin</start>
      <end>^=end</end>
      <include>
        <context ref="escape"/>
        <context ref="def:in-comment"/>
      </include>
    </context>

    <context id="attribute-definitions" style-ref="attribute-definition">
      <keyword>property</keyword>
      <keyword>getter</keyword>
      <keyword>setter</keyword>
    </context>

    <context id="proc" style-ref="keyword">
      <match>-></match>
    </context>

    <context id="definitions" style-ref="keyword">
      <!-- highlight `keyword` but not `.keyword` -->
      <prefix>(?&lt;![\w\.])</prefix>
      <keyword>alias</keyword>
      <keyword>class</keyword>
      <keyword>module</keyword>
      <keyword>def</keyword>
      <keyword>undef</keyword>
      <keyword>struct</keyword>
      <keyword>enum</keyword>
      <keyword>lib</keyword>
      <keyword>fun</keyword>
      <keyword>union</keyword>
      <keyword>type</keyword>
      <keyword>macro</keyword>
    </context>

    <context id="object_functions" style-ref="function">
      <!-- highlight `.keyword` but not `keyword` -->
      <match extended="true">
        (?&lt;=[\.])(?:
          \b all? \b |
            \b any? \b |
            \b chunks \b |
            \b compact_map \b |
            \b count \b |
            \b cycle \b |
            \b each \b |
            \b each_cons \b |
            \b each_slice \b |
            \b each_with_index \b |
            \b each_with_object \b |
            \b find \b |
            \b first \b |
            \b first? \b |
            \b flat_map \b |
            \b grep \b |
            \b group_by \b |
            \b in_groups_of \b |
            \b includes? \b |
            \b index \b |
            \b index_by \b |
            \b join \b |
            \b map \b |
            \b map_with_index \b |
            \b max \b |
            \b max? \b |
            \b max_by \b |
            \b max_by? \b |
            \b max_of \b |
            \b max_of? \b |
            \b min \b |
            \b min? \b |
            \b min_by \b |
            \b min_by? \b |
            \b min_of \b |
            \b min_of? \b |
            \b minmax \b |
            \b minmax? \b |
            \b minmax_by \b |
            \b minmax_by? \b |
            \b minmax_of \b |
            \b minmax_of? \b |
            \b none? \b |
            \b one? \b |
            \b partition \b |
            \b product \b |
            \b reduce \b |
            \b reject \b |
            \b select \b |
            \b size \b |
            \b skip \b |
            \b skip_while \b |
            \b sum \b |
            \b take_while \b  )
        </match>
    </context>

    <context id="module-handlers" style-ref="module-handler">
      <keyword>require</keyword>
      <keyword>include</keyword>
      <keyword>load</keyword>
    </context>

    <context id="functions" style-ref="function">
      <match extended="true">
        \b is_a\? |
        \b nil\? |
        \b as\? |
        \b as \b |
        \b typeof \b |
        \b responds_to\? |
        \b exit \b |
        \b puts \b |
        \b p \b |
        \b pp \b |
        \b pp! \b |
        \b to_(?:[a-z]|f(?:32|64)|i(?:8|16|32|64|128)|u(?:8|16|32|64|128)|yaml|json)\?? |
        \b as_(?:(?:.|f(?:32|64)|i(?:8|16|32|64|128)|u(?:8|16|32|64|128)|bytes|time|set)\??|nil) \b
      </match>
    </context>

    <context id="keywords-symbols" style-ref="keyword">
      <match extended="true">
        &amp;&amp; |
        \|\| |
        defined\?
      </match>
    </context>

    <context id="keywords" style-ref="keyword">
      <!-- highlight `keyword` but not `.keyword` -->
      <prefix>(?&lt;![\w\.])</prefix>
      <keyword>BEGIN</keyword>
      <keyword>END</keyword>
      <keyword>begin</keyword>
      <keyword>break</keyword>
      <keyword>case</keyword>
      <keyword>catch</keyword>
      <keyword>do</keyword>
      <keyword>else</keyword>
      <keyword>elsif</keyword>
      <keyword>end</keyword>
      <keyword>ensure</keyword>
      <keyword>for</keyword>
      <keyword>of</keyword>
      <keyword>if</keyword>
      <keyword>in</keyword>
      <keyword>loop</keyword>
      <keyword>next</keyword>
      <keyword>not</keyword>
      <keyword>private</keyword>
      <keyword>protected</keyword>
      <keyword>public</keyword>
      <keyword>redo</keyword>
      <keyword>rescue</keyword>
      <keyword>retry</keyword>
      <keyword>return</keyword>
      <keyword>super</keyword>
      <!-- <keyword>then</keyword> -->
      <keyword>throw</keyword>
      <keyword>uninitialized</keyword>
      <keyword>unless</keyword>
      <keyword>until</keyword>
      <keyword>when</keyword>
      <keyword>while</keyword>
      <keyword>yield</keyword>
    </context>

    <context id="builtins" style-ref="builtin">
      <keyword>Int</keyword>
      <keyword>Int128</keyword>
      <keyword>Int16</keyword>
      <keyword>Int32</keyword>
      <keyword>Int64</keyword>
      <keyword>Int8</keyword>
      <keyword>Bool</keyword>
      <keyword>Nil</keyword>
      <keyword>Tuple</keyword>
      <keyword>Class</keyword>
      <keyword>Enum</keyword>
      <keyword>Array</keyword>
      <keyword>BigDecimal</keyword>
      <keyword>BigFloat</keyword>
      <keyword>BigInt</keyword>
      <keyword>BigRational</keyword>
      <keyword>BitArray</keyword>
      <keyword>Char</keyword>
      <keyword>Float</keyword>
      <keyword>Float32</keyword>
      <keyword>Float64</keyword>
      <keyword>Struct</keyword>
      <keyword>Symbol</keyword>
      <keyword>UInt128</keyword>
      <keyword>UInt16</keyword>
      <keyword>UInt32</keyword>
      <keyword>UInt64</keyword>
      <keyword>UInt8</keyword>
      <keyword>Union</keyword>
      <keyword>NamedTuple</keyword>
      <keyword>Number</keyword>
      <keyword>Proc</keyword>
      <keyword>Pointer</keyword>
      <keyword>Regex</keyword>
      <keyword>Range</keyword>
      <keyword>Atomic</keyword>
      <keyword>Complex</keyword>
      <keyword>Spec</keyword>
      <keyword>Set</keyword>

      <keyword>Adler32</keyword>
      <keyword>ArgumentError</keyword>

      <keyword>Base64</keyword>
      <keyword>Benchmark</keyword>
      <keyword>Box</keyword>
      <keyword>Bytes</keyword>
      <keyword>Channel</keyword>
      <keyword>Colorize</keyword>
      <keyword>Comparable</keyword>

      <keyword>Concurrent</keyword>
      <keyword>CRC32</keyword>
      <keyword>Crypto</keyword>
      <keyword>Crystal</keyword>
      <keyword>CSV</keyword>
      <keyword>Debug</keyword>
      <keyword>Deque</keyword>
      <keyword>Digest</keyword>
      <keyword>Dir</keyword>
      <keyword>DivisionByZero</keyword>
      <keyword>DL</keyword>
      <keyword>ECR</keyword>
      <keyword>Enumerable</keyword>
      <keyword>ENV</keyword>
      <keyword>Errno</keyword>
      <keyword>Exception</keyword>
      <keyword>Fiber</keyword>
      <keyword>File</keyword>
      <keyword>FileUtils</keyword>
      <keyword>Flate</keyword>
      <keyword>GC</keyword>
      <keyword>Gzip</keyword>
      <keyword>Hash</keyword>
      <keyword>HTML</keyword>
      <keyword>HTTP</keyword>
      <keyword>Indexable</keyword>
      <keyword>IndexError</keyword>
      <keyword>INI</keyword>
      <keyword>InvalidBigDecimalException</keyword>
      <keyword>InvalidByteSequenceError</keyword>
      <keyword>IO</keyword>
      <keyword>IPSocket</keyword>
      <keyword>Iterable</keyword>
      <keyword>Iterator</keyword>
      <keyword>JSON</keyword>
      <keyword>KeyError</keyword>
      <keyword>Levenshtein</keyword>
      <keyword>LLVM</keyword>
      <keyword>Logger</keyword>
      <keyword>Markdown</keyword>
      <keyword>Math</keyword>
      <keyword>Mutex</keyword>
      <keyword>OAuth</keyword>
      <keyword>OAuth2</keyword>
      <keyword>Object</keyword>
      <keyword>OpenSSL</keyword>
      <keyword>OptionParser</keyword>
      <keyword>PartialComparable</keyword>
      <keyword>PrettyPrint</keyword>

      <keyword>Process</keyword>
      <keyword>Random</keyword>

      <keyword>Readline</keyword>
      <keyword>Reference</keyword>
      <keyword>Reflect</keyword>

      <keyword>Signal</keyword>
      <keyword>Slice</keyword>
      <keyword>Socket</keyword>

      <keyword>StaticArray</keyword>
      <keyword>String</keyword>
      <keyword>StringPool</keyword>
      <keyword>StringScanner</keyword>
      <keyword>System</keyword>
      <keyword>TCPServer</keyword>
      <keyword>TCPSocket</keyword>
      <keyword>Tempfile</keyword>
      <keyword>Termios</keyword>
      <keyword>Time</keyword>
      <keyword>TypeCastError</keyword>
      <keyword>UDPSocket</keyword>
      <keyword>UNIXServer</keyword>
      <keyword>UNIXSocket</keyword>
      <keyword>URI</keyword>
      <keyword>UUID</keyword>
      <keyword>Value</keyword>
      <keyword>WeakRef</keyword>
      <keyword>XML</keyword>
      <keyword>YAML</keyword>
      <keyword>Zip</keyword>
      <keyword>Zlib</keyword>
    </context>

    <context id="special-variables" style-ref="special-variable">
      <keyword>self</keyword>
      <keyword>super</keyword>
      <keyword>__FILE__</keyword>
      <keyword>__LINE__</keyword>
      <keyword>__END_LINE__</keyword>
      <keyword>__DIR__</keyword>
      <keyword>ARGV</keyword>
      <keyword>STDIN</keyword>
      <keyword>STDOUT</keyword>
      <keyword>STDERR</keyword>
    </context>

    <context id="predefined-variables" style-ref="predefined-variable">
      <match extended="true">
        \$([!$&amp;"'*+,./0:;&lt;=&gt;?@\`~1-9]|
           -[0FIKadilpvw]|
           (deferr|defout|stderr|stdin|stdout|
            DEBUG|FILENAME|KCODE|LOADED_FEATURES|LOAD_PATH|
            PROGRAM_NAME|SAFE|VERBOSE)\b)
      </match>
    </context>

    <context id="global-variables" style-ref="variable">
      <match>\$[a-zA-Z_][a-zA-Z0-9_]*</match>
    </context>

    <context id="class-variables" style-ref="variable">
      <match>@@[a-zA-Z_][a-zA-Z0-9_]*</match>
    </context>

    <context id="instance-variables" style-ref="variable">
      <match>@[a-zA-Z_][a-zA-Z0-9_]*</match>
    </context>

    <context id="symbols" style-ref="symbol">
      <match>(?&lt;!:):[a-zA-Z0-9_]+</match>
    </context>

    <context id="regexp-variables" style-ref="regex">
      <match>\$[1-9][0-9]*</match>
    </context>

    <context id="constants" style-ref="constant">
      <match>(::)?\b[A-Z][A-Za-z0-9_]*\b</match>
    </context>

    <context id="nil-value" style-ref="nil-value">
      <keyword>NIL</keyword>
      <keyword>nil</keyword>
    </context>

    <context id="boolean" style-ref="boolean">
      <keyword>FALSE</keyword>
      <keyword>TRUE</keyword>
      <keyword>false</keyword>
      <keyword>true</keyword>
    </context>

    <define-regex id="underscore_num">\d(_?\d)*</define-regex>

    <define-regex id="float" extended="true">
      ( \%{underscore_num}\.\%{underscore_num} ) |
      ( (\%{underscore_num}(\.\%{underscore_num})?)[eE][+-]?\%{underscore_num} )
    </define-regex>

    <context id="float" style-ref="floating-point">
      <match>(?&lt;![\w\.])\%{float}(?![\w\.])</match>
    </context>

    <context id="decimal" style-ref="decimal">
      <match>(?&lt;![\w])([1-9](_?[0-9])*|0)(?![\w])</match>
    </context>

    <context id="hex" style-ref="base-n-integer">
      <match>(?&lt;![\w\.])0[xX][0-9A-Fa-f](_?[0-9A-Fa-f])*(?![\w\.])</match>
    </context>

    <context id="octal" style-ref="base-n-integer">
      <match>(?&lt;![\w\.])0[0-7](_?[0-7])*(?![\w\.])</match>
    </context>

    <context id="binary" style-ref="base-n-integer">
      <match>(?&lt;![\w\.])0[bB][01](_?[01])*(?![\w\.])</match>
    </context>

    <context id="numeric-literal" style-ref="numeric-literal">
      <match>(?&lt;![\w\.])\?((\\[MC]-){1,2}|\\?)\S</match>
    </context>

    <!-- in double quotes and backticks -->
    <context id="simple-interpolation">
      <start>#(?=[@$])</start> <!-- need assertion to not highlight single # -->
      <end></end>
      <include>
        <context ref="class-variables"/>
        <context ref="instance-variables"/>
        <context ref="global-variables"/>
      </include>
    </context>

    <!-- in double quotes and backticks -->
    <!-- FIXME: really would like for the syntax highlight to go back
         to none here, as any ruby code could go here -->
    <context id="complex-interpolation" class-disabled="string">
      <start>#{</start>
      <end>}</end>
      <include>
        <context ref="crystal:*"/>
      </include>
    </context>

    <context id="inside-interpolated-string">
      <include>
        <context ref="escape"/>
        <context ref="def:line-continue"/>
        <context ref="complex-interpolation"/>
        <context ref="simple-interpolation"/>
      </include>
    </context>

    <!-- ruby strings do not end at line end,
         so we cannot use def:string
         (parts lifted from perl.lang) -->
    <context id="double-quoted-string" style-ref="string" class="string" class-disabled="no-spell-check">
      <start>"</start>
      <end>"</end>
      <include>
        <context ref="inside-interpolated-string"/>
      </include>
    </context>

    <context id="char" style-ref="char">
      <match>'(\\(.+)|.)'</match>
    </context>

    <context id="override-sh-variable-definition">
      <match>(^\s*|`\s*|(?&lt;=then|else|do|export)\s+)([a-zA-Z_][a-zA-Z0-9_]*)\=</match>
      <include>
        <context sub-pattern="2" style-ref="variable-definition"/>
      </include>
    </context>

    <context id="backtick">
      <start>`</start>
      <end>`</end>
      <include>
        <context ref="inside-interpolated-string"/>
        <context ref="sh:line-comment"/>
        <context ref="sh:single-quoted-string"/>
        <context ref="sh:double-quoted-string"/>
        <context ref="sh:subshell"/>
        <context ref="sh:case"/>
        <context ref="sh:punctuator"/>
        <context ref="sh:function"/>
        <context ref="sh:here-doc"/>
        <context ref="sh:redirection"/>
        <context ref="sh:operator"/>
        <context ref="sh:variable"/>
        <context ref="override-sh-variable-definition"/>
        <context ref="sh:built-in-command"/>
        <context ref="sh:common-command"/>
      </include>
    </context>

    <context id="here-doc-string" style-ref="here-doc" style-inside="true">
      <start>&lt;&lt;([a-zA-Z_]\w*)</start>
      <end>^\%{1@start}</end>
      <include>
        <context ref="inside-interpolated-string"/>
      </include>
    </context>

    <context id="here-doc-indented-string" style-ref="here-doc" style-inside="true">
      <start>&lt;&lt;-([a-zA-Z_]\w*)</start>
      <end>^\s*\%{1@start}</end>
      <include>
        <context ref="inside-interpolated-string"/>
      </include>
    </context>

    <context id="here-doc-single-quoted-string" style-ref="here-doc" style-inside="true">
      <start>&lt;&lt;'(\w+)'</start>
      <end>^\%{1@start}</end>
      <include>
        <context sub-pattern="0" where="start" style-ref="here-doc-bound"/>
        <context sub-pattern="0" where="end" style-ref="here-doc-bound"/>
      </include>
    </context>

    <context id="here-doc-double-quoted-string" style-ref="here-doc" style-inside="true">
      <start>&lt;&lt;"(\w+)"</start>
      <end>^\%{1@start}</end>
      <include>
        <context sub-pattern="0" where="start" style-ref="here-doc-bound"/>
        <context sub-pattern="0" where="end" style-ref="here-doc-bound"/>
        <context ref="inside-interpolated-string"/>
      </include>
    </context>

    <context id="macro-multi-line">
      <start>{[%]</start>
      <end>[%]}</end>
      <include>
        <context sub-pattern="0" where="start" style-ref="macro"/>
        <context sub-pattern="0" where="end" style-ref="macro"/>
        <context ref="crystal:*"/>
      </include>
    </context>

    <context id="macro-node">
      <start>{{</start>
      <end>}}</end>
      <include>
        <context sub-pattern="0" where="start" style-ref="macro"/>
        <context sub-pattern="0" where="end" style-ref="macro"/>
        <context ref="crystal:*"/>
      </include>
    </context>

    <context id="attribute">
      <start>@\[</start>
      <end>\]</end>
      <include>
        <context sub-pattern="0" where="start" style-ref="attribute"/>
        <context sub-pattern="0" where="end" style-ref="attribute"/>
        <context ref="crystal:*"/>
      </include>
    </context>

    <context id="interpolated-literal">
      <include>
        <context style-ref="string" style-inside="true">
          <start>[%][QWx]?\(</start>
          <end>\)</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context ref="inside-interpolated-string"/>
            <context>
              <start>\(</start>
              <end>\)</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true" >
          <start>[%][QWx]?\[</start>
          <end>]</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context ref="inside-interpolated-string"/>
            <context>
              <start>\[</start>
              <end>]</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true">
          <start>[%][QWx]?{</start>
          <end>}</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context ref="inside-interpolated-string"/>
            <context>
              <start>{</start>
              <end>}</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true">
          <start>[%][QWx]?&lt;</start>
          <end>&gt;</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context ref="inside-interpolated-string"/>
            <context>
              <start>&lt;</start>
              <end>&gt;</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true">
          <start>[%][QWx]?([^[:alnum:]{}&lt;[(])</start>
          <end>\%{1@start}</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context ref="inside-interpolated-string"/>
          </include>
        </context>
      </include>
    </context>

    <context id="non-interpolated-literal">
      <include>
        <context style-ref="string" style-inside="true">
          <start>[%][qsw]\(</start>
          <end>\)</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context style-ref="escape">
              <match>\\['\\]</match>
            </context>
            <context>
              <start>\(</start>
              <end>\)</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true">
          <start>[%][qsw]\[</start>
          <end>]</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context style-ref="escape">
              <match>\\['\\]</match>
            </context>
            <context>
              <start>\[</start>
              <end>]</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true">
          <start>[%][qsw]\{</start>
          <end>\}</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context style-ref="escape">
              <match>\\['\\]</match>
            </context>
            <context>
              <start>\{</start>
              <end>\}</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true">
          <start>[%][qsw]&lt;</start>
          <end>&gt;</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context style-ref="escape">
              <match>\\['\\]</match>
            </context>
            <context>
              <start>&lt;</start>
              <end>&gt;</end>
            </context>
          </include>
        </context>

        <context style-ref="string" style-inside="true">
          <start>[%][qsw]([^[:alnum:]{&lt;[(])</start>
          <end>\%{1@start}</end>
          <include>
            <context where="start" sub-pattern="0" style-ref="literal"/>
            <context where="end" sub-pattern="0" style-ref="literal"/>

            <context style-ref="escape">
              <match>\\['\\]</match>
            </context>
          </include>
        </context>
      </include>
    </context>

    <define-regex id="regex-opts">[iomx]*[neus]?[iomx]*</define-regex>

    <context id="regex-bracketed" style-ref="escape" style-inside="true">
      <start>(?&lt;!\\)[[]</start>
      <end>(?&lt;!\\)]</end>
    </context>

    <context id="regex-alt-form" style-ref="regex">
      <start>\%r\|</start>
      <end>\|\%{regex-opts}</end>
      <include>
        <context ref="escape"/>
        <context ref="def:line-continue"/>
        <context ref="complex-interpolation"/>
        <context ref="simple-interpolation"/>
        <context ref="regex-bracketed"/>
       </include>
     </context>

    <context id="regex-simple" style-ref="regex">
      <start extended="true">
        ((?&lt;=([(]|\s))|^)
        \/
        (?=
          ([^/\\]*(\\.))*
          [^/]*
          \/
          \%{regex-opts}
          ([),;.]|\s|$)
        )</start>
      <end>\/\%{regex-opts}</end>
      <include>
        <context ref="escape"/>
        <context ref="def:line-continue"/>
        <context ref="complex-interpolation"/>
        <context ref="simple-interpolation"/>
        <context ref="regex-bracketed"/>
      </include>
    </context>

    <context id="crystal" class="no-spell-check">
      <include>
        <context ref="backtick"/>
        <context ref="def:shebang"/>
        <context ref="def:shell-like-comment"/>
        <context ref="multiline-comment"/>
        <context ref="here-doc-single-quoted-string"/>
        <context ref="here-doc-double-quoted-string"/>
        <context ref="here-doc-string"/>
        <context ref="here-doc-indented-string"/>
        <context ref="double-quoted-string"/>
        <context ref="char"/>
        <context ref="attribute-definitions"/>
        <context ref="definitions"/>
        <context ref="module-handlers"/>
        <context ref="proc"/>
        <context ref="keywords"/>
        <context ref="keywords-symbols"/>
        <context ref="builtins"/>
        <context ref="functions"/>
        <context ref="object_functions"/>
        <context ref="special-variables"/>
        <context ref="predefined-variables"/>
        <context ref="global-variables"/>
        <context ref="class-variables"/>
        <context ref="instance-variables"/>
        <context ref="symbols"/>
        <context ref="regexp-variables"/>
        <context ref="constants"/>
        <context ref="nil-value"/>
        <context ref="boolean"/>
        <context ref="float"/>
        <context ref="decimal"/>
        <context ref="hex"/>
        <context ref="octal"/>
        <context ref="binary"/>
        <context ref="numeric-literal"/>
        <context ref="regex-alt-form"/>
        <context ref="regex-simple"/>
        <context ref="attribute"/>
        <context ref="macro-multi-line"/>
        <context ref="macro-node"/>
        <context ref="non-interpolated-literal"/>
        <context ref="interpolated-literal"/>
      </include>
    </context>

  </definitions>
</language>
